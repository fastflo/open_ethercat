#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#define GRANT_CAP_NET_RAW_PROCFS "/proc/grant_cap_net_raw"

int try_grant_cap_net_raw_init() {
  if(access(GRANT_CAP_NET_RAW_PROCFS, R_OK) != 0)
    return 0; // does probably not exist or is not readable
  
  int fd = open(GRANT_CAP_NET_RAW_PROCFS, O_RDONLY);
  if(fd == -1) {
    perror("open");
    return -1;
  }
  char buffer[1024];
  int n = read(fd, buffer, 1024);
  close(fd);
  if(n <= 0 || strncmp(buffer, "OK", 2))
    return -1;
  return 0;
}

int main(int argc, char* argv[]) {
  if(try_grant_cap_net_raw_init() == -1) {
    fprintf(stderr, "error: grant_cap_net_raw_init failed!\n");
    return -1;
  }

    
  int fd = socket(AF_PACKET, SOCK_RAW, 0);
  printf("packet socket fd: %d\n", fd);
  if(fd == -1) {
    perror("socket");
    return -1;
  }
  printf("have AF_PACKET socket\n");

  if (argc < 2) {
    printf("need interface name as argument to send test-frame\n");
    return 0;
  }
  char if_name[IFNAMSIZ];
  strcpy(if_name, argv[1]);

  /* Get the index of the interface to send on */
  struct ifreq if_idx;
  memset(&if_idx, 0, sizeof(struct ifreq));
  strncpy(if_idx.ifr_name, if_name, IFNAMSIZ-1);
  if(ioctl(fd, SIOCGIFINDEX, &if_idx) < 0) {
    perror("SIOCGIFINDEX");
    return -1;
  }
  /* Get the MAC address of the interface to send on */
  struct ifreq if_mac;
  memset(&if_mac, 0, sizeof(struct ifreq));
  strncpy(if_mac.ifr_name, if_name, IFNAMSIZ-1);
  if(ioctl(fd, SIOCGIFHWADDR, &if_mac) < 0) {
    perror("SIOCGIFHWADDR");
    return -1;
  }

  uint8_t dummy_packet[sizeof(struct ether_header) + 10];
  struct ether_header *eh = (struct ether_header*) dummy_packet;
  memcpy(eh->ether_shost, &if_mac.ifr_hwaddr.sa_data, 6);
  memset(eh->ether_dhost, 0xff, 6);
  eh->ether_type = htons(0x4321); // ETH_P_IP
  memset(eh+1, 0x42, 10);

  struct sockaddr_ll addr;
  memset(&addr, 0, sizeof(addr));
  addr.sll_ifindex = if_idx.ifr_ifindex;
  addr.sll_halen = ETH_ALEN;
  memcpy(addr.sll_addr, eh->ether_dhost, 6);

  int n = sendto(fd, &dummy_packet, sizeof(dummy_packet), 0, (struct sockaddr*)&addr, sizeof(addr));
  if(n == -1) {
    perror("send");
    return -1;
  }
  printf("send: %d bytes done.\n", n);
  
  return 0;
}
